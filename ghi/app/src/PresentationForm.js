import React, { useEffect, useState } from 'react';


function PresentationForm() {
    const [conferences, setPresentation] = useState([]);


    // Change Presenter Name form event handling
    const [presenterName, setPresenterName] = useState("");
    const handlePresenterNameChange = (event) => {
        const value = event.target.value;
        setPresenterName(value);
    }


    // Change Email form event handling
    const [presenterEmail, setEmail] = useState("");
    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    }


    // Change Company form event handling
    const [companyName, setCompanyName] = useState("");
    const handleCompanyChange = (event) => {
        const value = event.target.value;
        setCompanyName(value);
    }


    // Change Title form event handling
    const [title, setTitle] = useState("");
    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
    }


    // Change Synopsis form event handling
    const [synopsis, setSynopsis] = useState("");
    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }


    // Change Conference form event handling
    const [conference, setConference] = useState("");
    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }


    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setPresentation(data.conferences);
        }
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.presenter_name = presenterName;
        data.presenter_email= presenterEmail;
        data.company_name = companyName;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = conference;

        // const cid = data.conference

        const presentatinonUrl = `http://localhost:8000/${conference.id}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentatinonUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            setPresenterName('');
            setEmail('');
            setCompanyName('');
            setTitle('');
            setSynopsis('');
            setConference('');
        }
    }


    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new presentation</h1>
                    <form onSubmit={handleSubmit} id="create-presentation-form">
                    <div className="form-floating mb-3">
                        <input onChange={handlePresenterNameChange} value={presenterName} placeholder="Name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleEmailChange} value={presenterEmail} placeholder="Email" required type="email" name="presenter_email" id="presenter_email" className="form-control" />
                        <label htmlFor="email">Email</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleCompanyChange} value={companyName} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control" />
                        <label htmlFor="company">Company name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleTitleChange} value={title} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                        <label htmlFor="title">Title</label>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="synopsis"  className="form-label">Synopsis</label>
                        <textarea onChange={handleSynopsisChange} value={synopsis} placeholder="Brief synopsis of the presentation"id="synopsis" rows="3" name="synopsis" className="form-control"></textarea>
                    </div>
                    <div className="mb-3">
                        {/*add  the options changes */}
                        <select onChange={handleConferenceChange} value={conference} required name="conference" id="conference" className="form-select">
                        <option value="">Choose a conference</option>
                        {conferences.map(conference => {
                            return (
                                <option key={conference.id} value={conference.id}>
                                    {conference.name}
                                </option>
                            );
                        })}
                        </select>

                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}


export default PresentationForm;