import Nav from './Nav';
import MainPage from "./MainPage";
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './attend-conference';
import PresentationForm from './PresentationForm';
import { HashRouter, Routes, Route } from "react-router-dom";
import ConferenceList from './ConferenceList';


function App() {

  return (
    <>
    <HashRouter>
      <Nav />
      <Routes>
        <Route index element={<MainPage />} />
        <Route path="locations">
          <Route path="new" element={<LocationForm />} />
        </Route>
        <Route path="conferences">
          <Route index element={<ConferenceList />} />
          <Route path="new" element={<ConferenceForm />} />
        </Route>
        <Route path="attendees">
          <Route index element={<AttendeesList />} />
          <Route path="new" element={<AttendConferenceForm />} />
        </Route>
        <Route path="presentations">
          <Route path="new" element={<PresentationForm />} />
        </Route>
      </Routes>
    </HashRouter>
    </>
  );
}

export default App;
