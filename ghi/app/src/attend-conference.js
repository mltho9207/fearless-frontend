import React, { useEffect, useState } from 'react';


function AttendConferenceForm() {
  const [conferences, setConferences] = useState([]);
  const [showSuccessfulSubmit, setShowSuccessfulSubmit] = useState(false);


  // Change Location form event handling
  const [conference, setConference] = useState("");
  const handleConferenceChange = (event) => {
    const value = event.target.value;
    setConference(value);
  }


  // Change Name form event handling
  const [name, setName] = useState("");
  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }


  // Change Email form event handling
  const [email, setEmail] = useState("");
  const handleEmailChange = (event) => {
    const value = event.target.value;
    setEmail(value);
  }


  const fetchData = async () => {
      const url = 'http://localhost:8000/api/conferences/';
      const response = await fetch(url);
      if (response.ok) {
          const data = await response.json();
          setConferences(data.conferences);
      }
  }


  const handleSubmit = async (event) => {
      event.preventDefault();
      const data = {};
      data.email = email;
      data.name = name;
      data.conference = conference;
      console.log(data);

      const conferencesUrl = 'http://localhost:8001/api/attendees/';
      const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
              'Content-Type': 'application/json',
          },
      };

      const response = await fetch(conferencesUrl, fetchConfig);
      if (response.ok) {
        const newConferenceAttendee = await response.json();
          setConference('');
          setName('');
          setEmail('');
          setShowSuccessfulSubmit(true);
      }
  }


  useEffect(() => {
      fetchData();
  }, []);


 // CSS classes for rendering
  let spinnerClasses = 'd-flex justify-content-center mb-3';
  let dropdownClasses = 'form-select d-none';
  if (conferences.length > 0) {
    spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
    dropdownClasses = 'form-select';
  }

  let formClasses = '';



  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col col-sm-auto">
          <div className="shadow bg-light p-4 mt-4">
            <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4"  src="/logo.svg" alt="Logo" />
          </div>
        </div>

        <div className="col-8 shadow bg-light p-4 mt-4">
          {showSuccessfulSubmit ? (
            <div className="alert alert-success mt-5" role="alert">
              Merry Christmas you filthy animals. And a Happy New Year.
            </div>
        ) : (
          <div>
            <h1>It's Conference Time!</h1>

            <div>
              <p>Please choose which conference you'd like to attend.</p>
            </div>

            <div className={spinnerClasses} id="loading-conference-spinner">
              <div className="spinner-grow text-secondary" role="status">
                <span className="visually-hidden">Loading...</span>
              </div>
            </div>

            <form className ={formClasses} onSubmit={handleSubmit} id="create-conference-form">
              <div className="mb-3">
                <select onChange={handleConferenceChange} value={conference} required name="conference" id="conference" className={dropdownClasses}>
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
                    return (
                      <option key={conference.href} value={conference.href}>
                        {conference.name}
                      </option>
                    );
                    })}
                </select>
              </div>

              <div>
                <p>Now, tell us about yourself.</p>
              </div>

              <div className="row">
                <div className="col">
                  <div className="form-floating mb-3">
                    <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Your full name</label>
                  </div>
                </div>

                <div className="col">
                  <div className="form-floating mb-3">
                    <input onChange={handleEmailChange} value={email} placeholder="Email" required type="text" name="email" id="email" className="form-control" />
                    <label htmlFor="name">Your email address</label>
                  </div>
                </div>
              </div>

              <button className="btn btn-primary">I'm Going!</button>

            </form>
          </div>
          )}
        </div>
      </div>
    </div>
  );
}


export default AttendConferenceForm;