import { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal'

function ConferenceList() {
    const [conferences, setConferences] = useState([]);

    const [show, setShow] = useState(false);
    const[deleteConfirmId, setDeleteConfirmId] = useState(null);

    const handleDelete = (id) => {
      setDeleteConfirmId(id);
      setShow(true);
    }

    const handleClose = () => {
      setShow(false);
      setDeleteConfirmId(null);
    }

    const getData = async () => {
        const response = await fetch('http://localhost:8000/api/conferences/');

        if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences)
        }
    }

    useEffect(()=>{
        getData()
    }, [])


    const handleDeleteConfirm = async () => {
      const request = await fetch(`http://localhost:8000/api/conferences/${deleteConfirmId}`, { method: "DELETE"});
      const response = await request.json();
      getData();
      handleClose();
    }


    return (
      <>
        <table className="table table-striped">
            <thead>
                <tr>
                <th>Conf id</th>
                <th>Name</th>
                <th>Max attendees</th>
                <th>Starts</th>
                <th>Ends</th>
                <th>Where</th>
                </tr>
            </thead>
            <tbody>
                {conferences.sort((a,b) => (a.id - b.id)).map(conference => {
                return (
                    <tr key={conference.href}>
                    <td>{ conference.id }</td>
                    <td>{ conference.name }</td>
                    <td>{ conference.max_attendees }</td>
                    <td>{new Date(conference.starts).toLocaleDateString()}</td>
                    <td>{new Date(conference.ends).toLocaleDateString()}</td>
                    <td>{ conference.location.name }</td>
                    <td><button onClick = {() => {handleDelete(conference.id)}} className="btn btn-danger">Delete</button></td>
                    </tr>
                    );
                })}
            </tbody>
        </table>

        <Modal
          show={show}
          onHide={handleClose}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header closeButton>
          <Modal.Title>Confirm Deletion</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure you wish to delete this conference?
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={handleDeleteConfirm} variant="danger">
            Yes, Delete!
          </Button>
          <Button onClick={handleClose} variant="secondary">On, second thought...</Button>
        </Modal.Footer>
        </Modal>
      </>
    );
}

export default ConferenceList;