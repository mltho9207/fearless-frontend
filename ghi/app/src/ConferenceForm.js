import React, { useEffect, useState } from 'react';


function ConferenceForm() {
    const [locations, setLocations] = useState([]);


    // Change Name form event handling
    const [name, setName] = useState("");
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }


    // Change Start date form event handling
    const [starts, setStarts] = useState("");
    const handleStartsChange = (event) => {
        const value = event.target.value;
        setStarts(value);
    }


    // Change End date form event handling
    const [ends, setEnds] = useState("");
    const handleEndsChange = (event) => {
        const value = event.target.value;
        setEnds(value);
    }


    // Change Description form event handling
    const [description, setDescription] = useState("");
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }


    // Change Max Presentations form event handling
    const [maxPresentations, setMaxPresentations] = useState("");
    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }


    // Change Max Attendees form event handling
    const [maxAttendees, setMaxAttendees] = useState("");
    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }


    // Change Location form event handling
    const [location, setLocation] = useState("");
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }


    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;

        const conferencesUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferencesUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setMaxPresentations('');
            setMaxAttendees('');
            setLocation('');
        }
    }


    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleStartsChange} value={starts} required type="date" name="starts" id="starts" className="form-control" />
                        <label htmlFor="starts">Starts</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleEndsChange} value={ends} type="date" name="ends" id="ends" className="form-control" />
                        <label htmlFor="ends">Ends</label>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="description"  className="form-label">Description</label>
                        <textarea onChange={handleDescriptionChange} value={description} placeholder="Brief description of the conference." required type="textarea" name ="description" id="description" className="form-control" rows="6"></textarea>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleMaxPresentationsChange} value={maxPresentations} placeholder="Maximum Presentations" type="text" name="max_presentations" id="max_presentations" className="form-control"/>
                        <label htmlFor="max_presentations">Maximum Presentations</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleMaxAttendeesChange} value={maxAttendees} placeholder="Maximum Attendees" type="text" name="max_attendees" id="max_attendees" className="form-control"/>
                        <label htmlFor="max_attendees">Maximum Attendees</label>
                    </div>
                    <div className="mb-3">
                        {/*add  the options changes */}
                        <select onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select">
                        <option value="">Choose a location</option>
                        {locations.map(location => {
                            return (
                                <option key={location.id} value={location.id}>
                                    {location.id}. {location.name}
                                </option>
                            );
                        })}
                        </select>

                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}


export default ConferenceForm;